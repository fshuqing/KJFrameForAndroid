## ![logo](http://git.oschina.net/kymjs/KJFrameForAndroid/blob/master/doc/html_document/logo.jpg) KJFrameForAndroid简介
=================
[![Android Arsenal](http://img.shields.io/badge/Android%20Arsenal-KJFrameForAndroid-blue.svg?style=flat)](http://android-arsenal.com/details/1/836)
[![BlackDuck OpenHUB](https://www.openhub.net/p/KJFrameForAndroid/widgets/project_thin_badge.gif)](https://www.openhub.net/p/KJFrameForAndroid)
[![OSChina](https://www.oschina.net/img/logo_s2.gif)](http://www.oschina.net/p/kjframeforandroid)<br>
**KJFrameForAndroid** 又叫KJLibrary，是一个Android的快速开发工具包。同时封装了android中的Bitmap、Http、插件模块加载操作的框架，使开发者更容易轻松实现这些功能；<br>
KJFrameForAndroid的设计思想是通过封装Android原生SDK中复杂的复杂操作而达到简化Android应用级开发，最终实现快速而又安全高效的开发APP。我们的目标是用最少的代码，完成最多的操作，用最高的效率，完成最复杂的功能。<br>
## KJFrameForAndroid 相关链接
* blog：http://my.oschina.net/kymjs/blog<br>
* email：kymjs123@gmail.com<br>
* QQ群：[257053751](http://jq.qq.com/?_wv=1027&k=WoM2Aa)(开发者群1)，[201055521](http://jq.qq.com/?_wv=1027&k=MBVdpK)(开发者群2)
* 国内用户请访问GIT.OSC：[http://git.oschina.net/kymjs/KJFrameForAndroid](http://git.oschina.net/kymjs/KJFrameForAndroid)
* github项目地址：[https://github.com/kymjs/KJFrameForAndroid](https://github.com/kymjs/KJFrameForAndroid)
* 
* 留言版 [http://git.oschina.net/kymjs/KJFrameForAndroid/issues](http://git.oschina.net/kymjs/KJFrameForAndroid/issues)
* 版本日志 [http://git.oschina.net/kymjs/KJFrameForAndroid/blob/master/debug_log.txt](http://git.oschina.net/kymjs/KJFrameForAndroid/blob/master/debug_log.txt)

---
# 框架使用
**Demo工程运行** 
①[下载](http://git.oschina.net/kymjs/KJFrameForAndroid/repository/archive?ref=master)框架最新源码。
②选择KJLibraryExample工程导入Eclipse。
③将/binrary目录KJFrameForAndroid_Vxxx.jar包复制至demo的libs目录。
④删除[project.properties](http://git.oschina.net/kymjs/KJFrameForAndroid/blob/master/KJLibraryExample/project.properties)文件的最后一行<br>
**在项目中使用** ：将/binrary目录最新jar包[KJFrameForAndroid_xxx.jar](http://git.oschina.net/kymjs/KJFrameForAndroid/tree/master/binrary)添加到你工程/libs目录中并引用。<br>

* 由于使用了SDK最新的API函数，以及3.0版Fragment。KJFrameForAndroid框架最低支持API 11。<br>
*注：使用 KJFrameForAndroid 应用开发框架需要在你项目的AndroidManifest.xml文件中加入以下基本权限：*

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

----

## =======各模块使用介绍=======

## Plugin模块
使用Plugin模块可以让你的插件apk不用安装便直接被运行，极大的方便了APP动态更新，且可以轻松实现插件与APP项目之间的解耦。<br>
你可以在[这里](http://git.oschina.net/kymjs/KJFrameForAndroid/attach_files)获取项目示例中插件的Demo apk与源码。更多介绍请看[Plugin模块详细介绍](http://git.oschina.net/kymjs/KJFrameForAndroid/blob/master/PluginLibraryExplain.md)<br>
**现支持以下功能** 
* apk无需安装即可被应用调用<br>
* Activity的动态加载：包括生命周期和交互事件、R文件资源引用、插件与APP之间的数据通信<br>
* Fragment的完美加载使用<br>
* 动态注册的BroadcastReceiver<br>
* 绑定式、启动式Service均可完美使用<br>
* 已成功模拟出launchMode的效果。(launchModer实际上是一个虚拟的，生命周期的调用还是一样的，仅仅模拟出了系统的BackStack)<br>
* 完美集成了KJFrameForAndroid中UiLibrary->Topology的全部功能，支持注解式绑定控件设置监听<br>

## UILibrary模块
UILibrary包含两个部分Widget(控件)、Topology(Android框架结构继承链) [详细介绍...](http://my.oschina.net/kymjs/blog/284897)<br>

**UILibrary -> Widget控件部分**
主要封装了常用的UI控件，为了不让项目jar包过大，我们只引入了开发中一定会用到的控件，例如：可上下拉的KJScrollView、圆形显示的ImageView，可以双指缩放双击缩放双指旋转的ScaleImageView、等等......更多内容请自行查看项目文件中org.kymjs.kjframe.widget包下的内容<br>

**UILibrary -> Topology拓扑部分**
规范了Activity中数据及控件的初始化，并包含一个使用IOC设计思想的控件初始化方式：可通过注解的方式进行UI绑定，与设置监听，在Activity和Fragment中均可以通过一行代码绑定控件并实现点击监听；同时UILibrary为开发者定义了完善的KJActivity和KJFragment等基类，开发者只需手动继承就可以获得Topology部分的全部功能。<br>
```java
public class TabExample extends KJActivity {
    @BindView(id = R.id.bottombar_content1, click = true)
    public RadioButton mRbtn1;
    @BindView(id = R.id.bottombar_content2, click = true)
    private RadioButton mRbtn2;

    @Override
    public void setRootView() {
        setContentView(R.layout.aty_tab_example);
    }
    
    @Override
    protected void initWidget() {
        super.initWidget();
        mRbtn1.setText("控件已经初始化绑定并设置了监听");
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
        case R.id.bottombar_content1:
        ViewInject.toast("点击了mRbtn1");
            break;
        case R.id.bottombar_content2:
        ViewInject.toast("点击了mRbtn2");
            break;
        }
    }
}
```
###Topology中各函数调用顺序：
setRootView();<br>
@BindView<br>
initDataFromThread();（异步调用，可做耗时操作）<br>
threadDataInited();（initDataFromThread执行完成后才会回调）<br>
initData();<br>
initWidget();<br>
registerBroadcast();<br>


## BitmapLibrary模块
任何View(ImageView设置src，普通View设置bg)加载图片的时候都无需考虑图片加载过程中出现的oom和android容器快速滑动时候出现的图片错位等现象，同时无需考虑图片加载过程中出现的OOM。默认使用内存lru算法+磁盘lru算法缓存图片 [详细介绍](http://my.oschina.net/kymjs/blog/295001)<br>
**注：**在Android2.3之前，我们常常使用软引用或弱引用的形式去做缓存图片，然而根据Google的描述：垃圾回收器会更倾向于回收持有软引用或弱引用的对象，这让软引用和弱引用变得不再可靠。另外，Android 3.0 (API Level 11)中，图片的数据会存储在本地的内存当中，因而无法用一种可预见的方式将其释放，这就有潜在的风险造成应用程序的内存溢出并崩溃。BitmapLibrary使用lru算法去管理缓存，同时内存缓存配合磁盘缓存能更有效的管理缓存调用。
```java
KJBitmap kjb = KJBitmap.create();
/**
 * url不仅支持网络图片显示，同时支持本地SD卡上的图片显示；
 * view不仅可以是imageview，同时普通view也可以传入，框架会自动识别对imageview设置src对普通view设置bg
 */
// 载入本地图片
kjb.display(imageView, "file:///storage/sdcard0/1.jpg"); 
// 载入网络图片
kjb.display(textView, http://www.xxx.com/xxx.jpg); 
//自定义图片显示大小
kjb.display(view, http://www.xxx.com/xxx.jpg, 80, 80); //如不指定图片大小，默认采用控件大小显示图片
```

## HttpLibrary模块
KJLibrary默认对所有Http通信的数据做了缓存处理，缓存时间为5分钟。这么做的目的不仅是为了节省用户手机流量，同时是为了减少服务器压力<br>
HttpLibrary模块使用HttpUrlConnection实现方式实现网络通信、数据上传，使用HttpClient实现文件的断点下载。根据Google建议：在2.3系统之前由于HttpUrlConnection不稳定且有一定的BUG，应该尽量使用HttpClient；在2.3以后的系统，若只是简单的数据交互，应该使用更加轻量级、易扩展的HttpUrlConnection。<br>

###普通get、post方法示例：
```java
    	kjh.get("http://www.oschina.net/", new HttpCallBack();//与post相似，就只写一种了
		
		KJHttp kjh = new KJHttp();
		HttpParams params = new HttpParams();
        params.put("id", "1");
        params.put("name", "kymjs");
		kjh.post("http://192.168.1.149/post.php", params, new HttpCallBack() {
            @Override
            public void onPreStart() {
                super.onPreStart();
                KJLoger.debug("即将开始http请求");
            }
            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                ViewInject.longToast("请求成功");
                KJLoger.debug("请求成功:" + t.toString());
            }
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                KJLoger.debug("出现异常:" + strMsg);
            }
            @Override
            public void onFinish() {
                super.onFinish();
                KJLoger.debug("请求完成，不管成功还是失败");
            }
        });
```

###post上传文件方法示例：
```php
// 文件上传的PHP后台实现示例
<?php
    if ($_FILES["file"]["error"] > 0)
    {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else
    {
        echo "Upload: " . $_FILES["file"]["name"] . "<br />";
        echo "Type: " . $_FILES["file"]["type"] . "<br />";
        echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
    
		if (file_exists("upload/" . $_FILES["file"]["name"]))
        {
            echo $_FILES["file"]["name"] . " already exists. ";
        }
        else
        {
            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
            echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
        }
    }
?>
```
```java
	private void upload() {
       HttpParams params = new HttpParams();
		//支持一次传递多个文件，这里使用UtilsLibrary中的工具类获取一个文件
        params.put("file", FileUtils.getSaveFile("KJLibrary", "logo.jpg"));
        kjh.post("http://192.168.1.149/kymjs/hello.php", params,
                new HttpCallBack() {
                    @Override
                    public void onSuccess(String t) {
                        super.onSuccess(t);
                        ViewInject.toast("文件上传完成");
                    }

                    @Override
                    public void onFailure(Throwable t, int errorNo,
                            String strMsg) {
                        super.onFailure(t, errorNo, strMsg);
                        ViewInject.toast("文件上传失败" + strMsg);
                    }
                });
    }
```

###断点下载方法示例：
```java
	kjh.download(mEtDownloadPath.getText().toString(), FileUtils.getSaveFile("KJLibrary", "l.pdf"),
            new HttpCallBack() {
                @Override
                public void onSuccess(File f) {
                    super.onSuccess(f);
                    KJLoger.debug("success");
                    ViewInject.toast("下载成功");
                    mProgress.setProgress(mProgress.getMax());
                }
				
                @Override
                public void onFailure(Throwable t, int errorNo,
                        String strMsg) {
                    super.onFailure(t, errorNo, strMsg);
                    KJLoger.debug("onFailure");
                }
					
				/* onLoading方法只在下载方法中有效，且每秒回调一次 */
                @Override
                public void onLoading(long count, long current) {
                    super.onLoading(count, current);
                    mProgress.setMax((int) count);
                    mProgress.setProgress((int) current);
                    KJLoger.debug(count + "------" + current);
                }
            });
```

## DBLibrary模块
包含了android中的orm框架，一行代码就可以进行增删改查。支持一对多，多对一等查询。<br>
DB模块，很大程度上参考了[finalDB](https://github.com/kymjs/afinal)的设计，并在此基础上完善了几乎全部的API注释，与更多可定制的DB操作<br>
```java
	//普通数据存储
	KJDB db = KJDB.create(this);
	User ugc = new User(); //这里需要注意的是User对象必须有id属性，或者有通过@ID注解的属性
	ugc.setEmail("kymjs123@gmail.com");
	ugc.setName("kymjs");
	db.save(ugc);
```
```java
	//一对多数据存储
	public class Parent{  //JavaBean
		private int id;
		@OneToMany(manyColumn = "parentId")
		private OneToManyLazyLoader<Parent ,Child> children;
		/*....*/
	}
	public class Child{ //JavaBean
		private int id;
		private String text;
		@ManyToOne(column = "parentId")
		private  Parent  parent;
		/*....*/
	}
	List<Parent> all = db.findAll(Parent.class);
		for( Parent  item : all){
			if(item.getChildren ().getList().size()>0)
				ViewInject.toast(item.getText() + item.getChildren().getList().get(0).getText());
		}

```

## 帮助我
我是张涛，中国深圳，Android高级工程师<br>
如果我的项目帮到了你，可否在你有能力的基础捐助我买书学习，以让我更有信心和能力回馈网友。<br>
[点这里参与我的众筹](https://shenghuo.alipay.com/send/payment/fill.htm) 我的支付宝账号[kymjs@foxmail.com](https://shenghuo.alipay.com/send/payment/fill.htm)<br>
书本计划清单：<br>
《[项目经理应该知道的97件事](http://item.jd.com/10794354.html)》   ￥39<br>
《[简约至上:交互式设计四策略](http://item.jd.com/1412828696.html)》  ￥21<br>
《[参与感：小米口碑营销内部手册](http://item.jd.com/11512713.html)》	￥42<br>
《[格拉德威尔经典系列](http://www.amazon.cn/%E6%A0%BC%E6%8B%89%E5%BE%B7%E5%A8%81%E5%B0%94%E7%BB%8F%E5%85%B8%E7%B3%BB%E5%88%97-%E5%BC%82%E7%B1%BB-%E7%9C%A8%E7%9C%BC%E4%B9%8B%E9%97%B4-%E5%BC%95%E7%88%86%E7%82%B9-%E9%80%86%E8%BD%AC-%E5%A4%A7%E5%BC%80%E7%9C%BC%E7%95%8C-%E9%A9%AC%E5%B0%94%E7%A7%91%E5%A7%86%E2%80%A2%E6%A0%BC%E6%8B%89%E5%BE%B7%E5%A8%81%E5%B0%94/dp/B00JXFD0XM/ref=sr_1_1?s=books&ie=UTF8&qid=1418830300&sr=1-1&keywords=%E6%A0%BC%E6%8B%89%E5%BE%B7%E7%BB%B4%E5%B0%94)》	￥132<br>
